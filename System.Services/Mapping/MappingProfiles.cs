using AutoMapper;
using System.Infrastructure.DTO;
using SystemTheLastBugSpa.Data.Entity;

namespace System.Infrastructure.Mapping
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<RolPermissions, RolPermissionsDTO>().ReverseMap();
     
        }

    }

}
